#' Calculate y value from fit
#'
#' @param x x value to predict y for
#' @param fit fit from para4_fit()
#'
#' @return predicted y values from fit
#' @export
#'
#' @examples y_value_from_fit(x, fit)
y_value_from_fit <- function(x, fit) {
  bottom <- coef(fit)[["bottom"]]
  top <- coef(fit)[["top"]]
  logec50 <- coef(fit)[["logec50"]]
  hillslope <- coef(fit)[["hillslope"]]

  bottom + (top - bottom) / (1 + 10^((logec50 - x) * hillslope))
}
